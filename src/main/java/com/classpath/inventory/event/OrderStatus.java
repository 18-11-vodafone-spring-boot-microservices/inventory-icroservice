package com.classpath.inventory.event;

public enum OrderStatus {
	ORDER_ACCEPTED,
	ORDER_DECLINED,
	ORDER_PENDING
}
